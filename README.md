# Вывод тестов

```
|TEST 1 STARTED|
[TEST - AVERAGE MALLOC]
<<Initializing heap at 0x4040000 with size 12271>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      12271     free   0000
<<Allocating block at 0x4040000 with size 6135>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000       6135    taken   0000
 0x4041808       6119     free   0000
<<Freeing block at 0x4040000>>
|TEST 1 PASSED|

|TEST 2 STARTED|
[TEST - MALLOC 5 BLOCKS ANS FREE 1]
<<Initializing heap at 0x4040000 with size 12271>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      12271     free   0000
<<Allocating block at 0x4040000 with size 818>>
<<Allocating block at 0x4040343 with size 1227>>
<<Allocating block at 0x404081f with size 1636>>
<<Allocating block at 0x4040e94 with size 2045>>
<<Allocating block at 0x40416a2 with size 2454>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000        818    taken   0000
 0x4040343       1227    taken   0000
 0x404081f       1636    taken   0000
 0x4040e94       2045    taken   0000
 0x40416a2       2454    taken   0000
 0x4042049       4006     free   0000
<<Freeing block at 0x404081f>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000        818    taken   0000
 0x4040343       1227    taken   0000
 0x404081f       1636     free   0000
 0x4040e94       2045    taken   0000
 0x40416a2       2454    taken   0000
 0x4042049       4006     free   0000
|TEST 2 PASSED|

|TEST 3 STARTED|
[TEST - MALLOC 8 BLOCKS ANS FREE 2]
<<Initializing heap at 0x4040000 with size 12271>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      12271     free   0000
<<Allocating block at 0x4040000 with size 596>>
<<Allocating block at 0x4040265 with size 766>>
<<Allocating block at 0x4040574 with size 936>>
<<Allocating block at 0x404092d with size 1106>>
<<Allocating block at 0x4040d90 with size 1276>>
<<Allocating block at 0x404129d with size 1446>>
<<Allocating block at 0x4041854 with size 1616>>
<<Allocating block at 0x4041eb5 with size 1786>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000        596    taken   0000
 0x4040265        766    taken   0000
 0x4040574        936    taken   0000
 0x404092d       1106    taken   0000
 0x4040d90       1276    taken   0000
 0x404129d       1446    taken   0000
 0x4041854       1616    taken   0000
 0x4041eb5       1786    taken   0000
 0x40425c0       2607     free   0000
<<Freeing block at 0x404129d>>
<<Freeing block at 0x4040574>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000        596    taken   0000
 0x4040265        766    taken   0000
 0x4040574        936     free   0000
 0x404092d       1106    taken   0000
 0x4040d90       1276    taken   0000
 0x404129d       1446     free   0000
 0x4041854       1616    taken   0000
 0x4041eb5       1786    taken   0000
 0x40425c0       2607     free   0000
|TEST 3 PASSED|

|TEST 4 STARTED|
[TEST - GROW HEAP]
<<Initializing heap at 0x4040000 with size 12271>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      12271     free   0000
<<Allocating block at 0x4040000 with size 61355>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      61355    taken   0000
 0x404efbc      12339     free   0000
<<Freeing block at 0x4040000>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      73711     free   0000
<<Freeing block at 0x4040000>>
|TEST 4 PASSED|

|TEST 5 STARTED|
[TEST - GROW HEAP IF MEMORY BLOCKED]
<<Initializing heap at 0x4040000 with size 12271>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      12271     free   0000
<<Allocating block at 0x4040000 with size 12271>>
<<Setting up memory block at 0x4043000 with size 8192>>
<<Allocating block at 0x7ff902b73000 with size 12271>>
 --- Heap ---
     start   capacity   status   contents
 0x4040000      12271    taken   0000
0x7ff902b73000      12271    taken   0000
|TEST 5 PASSED|

[[ALL TESTS PASSED]]

```