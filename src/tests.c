//
// Created by Lenovo on 27.12.2022.
//

#define _DEFAULT_SOURCE

#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 12271

#define heap_debug(heap) debug_heap(stderr, heap)

#define debug(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)

static void destroy_heap(void *heap, size_t lz) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = lz}).bytes);
}

static struct block_header *get_block_header(void *ptr) {
    return (struct block_header *) (ptr - offsetof(struct block_header, contents));
}



static bool micro_test_init_heap(struct block_header **heap, size_t *heap_size) {
    *heap = heap_init(*heap_size);
    debug( "<<Initializing heap at %p with size %zu>>\n", *heap, (*heap)->capacity.bytes);
    if (!*heap) {
        return false;
    }
    heap_debug(*heap);
    *heap_size = (*heap)->capacity.bytes;
    return true;
}

static bool micro_test_alloc(struct block_header *heap, size_t heap_size, void **alloc, size_t alloc_size) {
    *alloc = _malloc(alloc_size);
    const struct block_header *alloc_header = get_block_header(*alloc);
    debug( "<<Allocating block at %p with size %zu>>\n", alloc_header, alloc_header->capacity.bytes);
    if (!*alloc || get_block_header(*alloc)->capacity.bytes != alloc_size || get_block_header(*alloc)->is_free) {
        destroy_heap(heap, heap_size);
        return false;
    }
    return true;
}

static bool micro_test_free(void *alloc) {
    debug("<<Freeing block at %p>>\n", get_block_header(alloc));
    _free(alloc);
    if (!get_block_header(alloc)->is_free) {
        return false;
    }

    return true;
}

static bool test_malloc() {
    debug("[TEST - AVERAGE MALLOC]\n");

    struct block_header *heap;
    size_t heap_size = HEAP_SIZE;
    if (!micro_test_init_heap(&heap, &heap_size)) return false;

    void *alloc;
    size_t alloc_size = heap_size / 2;
    if (!micro_test_alloc(heap, heap_size, &alloc, alloc_size)) return false;
    heap_debug(heap);

    if (!micro_test_free(alloc)) return false;

    destroy_heap(heap, heap_size);

    return true;
}

#define test_malloc_free_n(MALLOC_LENGTH, FREE_LENGTH)                                 \
static bool test_malloc_##MALLOC_LENGTH##_free_##FREE_LENGTH() {                       \
    debug("[TEST - MALLOC %d BLOCKS ANS FREE %d]\n", MALLOC_LENGTH, FREE_LENGTH);      \
                                                                                       \
    srand(time(NULL));                                                                 \
                                                                                       \
    struct block_header *heap;                                                         \
    size_t heap_size = HEAP_SIZE;                                                      \
    if (!micro_test_init_heap(&heap, &heap_size)) return false;                        \
                                                                                       \
    size_t blocks_length = MALLOC_LENGTH;                                              \
    size_t alloc_size = heap_size / (2*blocks_length);                                 \
    size_t d = (2*heap_size/(blocks_length) - 2*alloc_size)/(blocks_length+1);         \
    void *blocks[blocks_length];                                                       \
    for (size_t i = 0; i < blocks_length; i++) {                                       \
        if (!micro_test_alloc(heap, heap_size, blocks + i, alloc_size+d*(i-1))) {      \
            return false;                                                              \
        }                                                                              \
    }                                                                                  \
    heap_debug(heap);                                                                  \
                                                                                       \
    for(size_t i = 0; i < (FREE_LENGTH);) {                                            \
        size_t free_index = rand() % blocks_length;                                    \
        if(get_block_header(blocks[free_index])->is_free) continue;                    \
        if (!micro_test_free(blocks[free_index])) return false;                        \
        i++;                                                                           \
    }                                                                                  \
    heap_debug(heap);                                                                  \
                                                                                       \
    destroy_heap(heap, heap_size);                                                     \
    return true;                                                                       \
}

test_malloc_free_n(5, 1)

test_malloc_free_n(8, 2)

static bool test_grow_heap() {
    debug("[TEST - GROW HEAP]\n");

    struct block_header *heap;
    size_t heap_size = HEAP_SIZE;
    if (!micro_test_init_heap(&heap, &heap_size)) return false;

    void *alloc;
    size_t alloc_size = heap_size * 5;
    if (!micro_test_alloc(heap, heap_size, &alloc, alloc_size) || get_block_header(alloc)!=heap) {
        destroy_heap(heap, heap_size);
        return false;
    }
    heap_debug(heap);
    if (!micro_test_free(alloc)) {
        destroy_heap(heap, heap_size);
        return false;
    }
    heap_debug(heap);

    if (!micro_test_free(alloc)) {
        return false;
    }

    destroy_heap(heap, heap->capacity.bytes);
    return true;
}

static bool test_not_extend_heap() {
    debug("[TEST - GROW HEAP IF MEMORY BLOCKED]\n");

    struct block_header *heap;
    size_t heap_size = HEAP_SIZE;
    if (!micro_test_init_heap(&heap, &heap_size)) return false;
    void *alloc;
    if (!micro_test_alloc(heap, heap_size, &alloc, heap_size)) return false;
    struct block_header *alloc_header = get_block_header(alloc);

    void *memory_trash_addr = (void *) (heap->contents) + alloc_header->capacity.bytes;
    const size_t memory_trash_size = REGION_MIN_SIZE;
    debug("<<Setting up memory block at %p with size %zu>>\n", memory_trash_addr, memory_trash_size);
    void *memory_trash = mmap(memory_trash_addr,
                              memory_trash_size,
                              PROT_READ | PROT_WRITE,
                              MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS,
                              -1,
                              0);
    if (memory_trash == MAP_FAILED) {
        munmap(memory_trash_addr, memory_trash_size);
        return false;
    }
    void *after_alloc;
    size_t alloc_size = HEAP_SIZE;
    if (!micro_test_alloc(heap, heap_size, &after_alloc, alloc_size) || get_block_header(after_alloc)==heap) {
        return false;
    }
    heap_debug(heap);
    munmap(memory_trash_addr, memory_trash_size);
    destroy_heap(heap, heap->capacity.bytes);
    return true;
}

typedef bool (*simple_test)(void);

static simple_test tests[] = {
        test_malloc,
        test_malloc_5_free_1,
        test_malloc_8_free_2,
        test_grow_heap,
        test_not_extend_heap
};

#define tests_size (sizeof(tests)/sizeof(simple_test))

void test() {
    size_t n = tests_size;

    bool success = true;
    for (size_t i = 0; i < n; i++) {
        fprintf(stderr, "|TEST %zu STARTED|\n", i + 1);
        if (!tests[i]()) {
            debug("|TEST %zu FAILED|\n", i+1);
            success = false;
        } else {
            fprintf(stderr, "|TEST %zu PASSED|\n\n", i+1);
        }
    }

    if(success) {
        debug("[[ALL TESTS PASSED]]\n");
    } else {
        debug("[[NOT ALL TESTS PASSED]]\n");
    }
}